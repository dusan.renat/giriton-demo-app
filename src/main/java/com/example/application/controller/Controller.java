package com.example.application.controller;

import com.example.application.dto.ExecutionResultDto;
import com.example.application.service.ExecutionResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {
    private final ExecutionResultService service;

    public Controller(@Autowired ExecutionResultService service) {
        this.service = service;
    }

    @GetMapping("/results")
    public List<ExecutionResultDto> getExecutionResults() {
        return service.getExecutionResults();
    }
}
