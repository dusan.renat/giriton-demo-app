package com.example.application.task;

public enum TaskStatus {
    NEW,
    RUNNING,
    SUCCESS,
    FAILED
}
