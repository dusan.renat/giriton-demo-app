package com.example.application.task;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Component
public class ResolveIPTask extends Task {
    private final WebClient client;

    public ResolveIPTask() {
        this.client = WebClient.create("http://ip.jsontest.com");
    }

    @Override
    public TaskInstance createInstance(Integer id) {
        return new ResolveIPTaskInstance(id);
    }

    public class ResolveIPTaskInstance extends TaskInstance {
        public ResolveIPTaskInstance(Integer id) {
            super(id);
        }

        @Override
        public void execute() {
            try {
                Thread.sleep(
                        Duration.of(
                                (long) (5 + Math.random() * 5),
                                ChronoUnit.SECONDS
                        )
                );
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            if (getId() % 5 == 0) {
                throw new InternalError("Something bad happened.");
            }

            ResponseEntity<IPResponse> response = client
                    .get()
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .toEntity(IPResponse.class)
                    .block();

            if (response.getStatusCode().is2xxSuccessful()) {
                System.out.println("Retrieved IP address is " + response.getBody().ip);
            }
        }
    }

    public record IPResponse(String ip) {}
}
