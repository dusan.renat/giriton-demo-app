package com.example.application.task;

public abstract class Task {

    abstract public TaskInstance createInstance(Integer id);

    public abstract class TaskInstance implements Runnable {
        private final Integer id;

        private TaskStatus status = TaskStatus.NEW;
        private String errorMessage;

        public TaskInstance(Integer id) {
            this.id = id;
        }

        public Integer getId() {
            return id;
        }

        public TaskStatus getStatus() {
            return status;
        }

        public void setStatus(TaskStatus status) {
            this.status = status;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (Throwable t) {
                errorMessage = t.getMessage();
                throw t;
            }
        }

        abstract void execute();
    }
}
