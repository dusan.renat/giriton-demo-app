package com.example.application.broadcast;

import com.example.application.task.TaskStatus;

public record TaskStatusChangeBroadcastMessage(Integer id, TaskStatus status) {}
