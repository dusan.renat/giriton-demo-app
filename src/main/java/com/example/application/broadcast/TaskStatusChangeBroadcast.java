package com.example.application.broadcast;

import com.example.application.task.Task;
import com.vaadin.flow.shared.Registration;

import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class TaskStatusChangeBroadcast {
    static Executor executor = Executors.newSingleThreadExecutor();

    static LinkedList<Consumer<TaskStatusChangeBroadcastMessage>> listeners = new LinkedList<>();

    public static synchronized Registration register(Consumer<TaskStatusChangeBroadcastMessage> listener) {
        listeners.add(listener);

        return () -> {
            synchronized (TaskStatusChangeBroadcast.class) {
                listeners.remove(listener);
            }
        };
    }

    public static synchronized void broadcast(TaskStatusChangeBroadcastMessage message) {
        for (Consumer<TaskStatusChangeBroadcastMessage> listener : listeners) {
            executor.execute(() -> listener.accept(message));
        }
    }

    public static synchronized void broadcast(Task.TaskInstance task) {
        broadcast(new TaskStatusChangeBroadcastMessage(task.getId(), task.getStatus()));
    }
}
