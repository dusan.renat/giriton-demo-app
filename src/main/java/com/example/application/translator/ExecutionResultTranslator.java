package com.example.application.translator;

import com.example.application.dto.ExecutionResultDto;
import com.example.application.model.ExecutionResult;

public class ExecutionResultTranslator {
    private ExecutionResultTranslator() {}

    public static ExecutionResultDto toDto(ExecutionResult r) {
        ExecutionResultDto result = new ExecutionResultDto();
        result.setErrorText(r.getErrorText());
        result.setRunSequence(r.getRunSequence());
        result.setExecDateTime(r.getExecDateTime());
        result.setRunSucceeded(r.getRunSucceeded());
        return result;
    }
}
