package com.example.application.view.main;

import com.example.application.broadcast.TaskStatusChangeBroadcast;
import com.example.application.broadcast.TaskStatusChangeBroadcastMessage;
import com.example.application.service.TaskExecutorService;
import com.example.application.task.ResolveIPTask;
import com.example.application.task.Task;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

@PageTitle("Main")
@Route(value = "")
@RouteAlias(value = "")
@Component
@UIScope
public class MainView extends HorizontalLayout implements Consumer<TaskStatusChangeBroadcastMessage> {
    private final TaskExecutorService taskExecutorService;
    private final ResolveIPTask resolveIPTask;

    private final Button newTaskButton;
    private final Grid<Task.TaskInstance> tasksGrid;

    Registration broadcasterRegistration;
    private final AtomicInteger idCounter = new AtomicInteger(1);

    public MainView(
            @Autowired TaskExecutorService taskExecutorService,
            @Autowired ResolveIPTask resolveIPTask
            ) {
        this.taskExecutorService = taskExecutorService;
        this.resolveIPTask = resolveIPTask;

        newTaskButton = new Button("New task");
        newTaskButton.addClickListener(e -> {
            try {
                taskExecutorService.addNewTask(resolveIPTask.createInstance(idCounter.getAndIncrement()));
                System.out.println(taskExecutorService.getTasks());
            } catch (TaskRejectedException ex) {
                Notification.show("Task couldn't be added.");
            }
        });
        newTaskButton.addClickShortcut(Key.ENTER);

        tasksGrid = new Grid<>(Task.TaskInstance.class, false);
        tasksGrid.addColumn(Task.TaskInstance::getId).setHeader("Task ID");
        tasksGrid.addColumn(Task.TaskInstance::getStatus).setHeader("Status");

        setMargin(true);
        setVerticalComponentAlignment(Alignment.END, newTaskButton, tasksGrid);

        add(newTaskButton, tasksGrid);
    }

    @Override
    public void accept(TaskStatusChangeBroadcastMessage message) {
        tasksGrid.setItems(taskExecutorService.getTasks());
        switch (message.status()) {
            case NEW -> Notification.show(String.format("Úloha %s zařazena do fronty.", message.id()));
            case RUNNING -> Notification.show(String.format("Úloha %s běží.", message.id()));
            case SUCCESS -> Notification.show(String.format("Úloha %s skončila úspěšně.", message.id()));
            case FAILED -> Notification.show(String.format("Úloha %s skončila chybou.", message.id()));
        }
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = TaskStatusChangeBroadcast.register(newMessage -> {
            ui.access(() -> accept(newMessage));
        });
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }
}
