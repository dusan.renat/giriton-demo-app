package com.example.application.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.time.ZonedDateTime;

@Entity
public class ExecutionResult {

    @Id
    @Column(nullable = false)
    private Integer runSequence;
    @Column(nullable = false)
    private ZonedDateTime execDateTime;
    @Column(nullable = false)
    private Boolean runSucceeded;
    @Column(nullable = true)
    private String errorText;

    public Integer getRunSequence() {
        return runSequence;
    }

    public void setRunSequence(Integer runSequence) {
        this.runSequence = runSequence;
    }

    public ZonedDateTime getExecDateTime() {
        return execDateTime;
    }

    public void setExecDateTime(ZonedDateTime execDateTime) {
        this.execDateTime = execDateTime;
    }

    public Boolean getRunSucceeded() {
        return runSucceeded;
    }

    public void setRunSucceeded(Boolean runSucceeded) {
        this.runSucceeded = runSucceeded;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }
}

