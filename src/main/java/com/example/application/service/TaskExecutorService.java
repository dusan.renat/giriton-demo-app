package com.example.application.service;

import com.example.application.task.Task;
import com.example.application.task.TaskStatus;
import com.example.application.broadcast.TaskStatusChangeBroadcast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Queue;
import java.util.concurrent.*;

@Service
public class TaskExecutorService {
    private final ExecutionResultService executionResultService;

    private final ConcurrentLinkedQueue<Task.TaskInstance> tasks = new ConcurrentLinkedQueue<>();

    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(
            5,
            5,
            0,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>()
    ) {
        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            if (r instanceof Task.TaskInstance task) {
                task.setStatus(TaskStatus.RUNNING);
                TaskStatusChangeBroadcast.broadcast(task);
            }
        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            if (r instanceof Task.TaskInstance task) {
                if (t == null) {
                    task.setStatus(TaskStatus.SUCCESS);
                    logger.info("Task {} finished successfully.", task.getId());
                } else {
                    task.setStatus(TaskStatus.FAILED);
                    logger.error("Task {} failed: {}", task.getId(), task.getErrorMessage());
                }
                TaskStatusChangeBroadcast.broadcast(task);
                tasks.remove(r);

                executionResultService.saveExecutionResult(task);
            }
        }
    };

    private final Logger logger = LoggerFactory.getLogger(TaskExecutorService.class);

    public TaskExecutorService(@Autowired ExecutionResultService executionResultService) {
        this.executionResultService = executionResultService;
    }

    public void addNewTask(Task.TaskInstance task) {
        tasks.add(task);
        TaskStatusChangeBroadcast.broadcast(task);
        executor.execute(task);
    }

    public Queue<Task.TaskInstance> getTasks() {
        return tasks;
    }
}
