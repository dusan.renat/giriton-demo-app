package com.example.application.service;

import com.example.application.dto.ExecutionResultDto;
import com.example.application.model.ExecutionResult;
import com.example.application.repository.ExecutionResultRepository;
import com.example.application.task.Task;
import com.example.application.task.TaskStatus;
import com.example.application.translator.ExecutionResultTranslator;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class ExecutionResultService {

    private final Logger logger = LoggerFactory.getLogger(ExecutionResultService.class);

    private final ExecutionResultRepository repository;

    public ExecutionResultService(ExecutionResultRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void saveExecutionResult(Task.TaskInstance task) {
        ExecutionResult r = new ExecutionResult();
        r.setRunSequence(task.getId());
        r.setExecDateTime(ZonedDateTime.now());
        r.setRunSucceeded(task.getStatus() == TaskStatus.SUCCESS);
        r.setErrorText(task.getErrorMessage());
        repository.save(r);

        logger.info("Execution result for task {} saved to DB. Total records in DB: {}", task.getId(), repository.count());
    }

    public List<ExecutionResultDto> getExecutionResults() {
        return repository
                .findAll()
                .stream()
                .map(ExecutionResultTranslator::toDto)
                .toList();
    }
}
