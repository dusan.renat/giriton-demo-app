package com.example.application.dto;

import java.time.ZonedDateTime;

public class ExecutionResultDto {

    private Integer runSequence;
    private ZonedDateTime execDateTime;
    private Boolean runSucceeded;
    private String errorText;

    public Integer getRunSequence() {
        return runSequence;
    }

    public void setRunSequence(Integer runSequence) {
        this.runSequence = runSequence;
    }

    public ZonedDateTime getExecDateTime() {
        return execDateTime;
    }

    public void setExecDateTime(ZonedDateTime execDateTime) {
        this.execDateTime = execDateTime;
    }

    public Boolean getRunSucceeded() {
        return runSucceeded;
    }

    public void setRunSucceeded(Boolean runSucceeded) {
        this.runSucceeded = runSucceeded;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }
}
